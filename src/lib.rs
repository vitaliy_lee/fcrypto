// pub mod errors;

// use crate::errors::Error;

// pub type Result<T> = std::result::Result<T, Error>;

use std::error::Error;

pub type Result<T> = std::result::Result<T, Box<dyn Error>>;

pub trait CSP {

    fn generate_random(&self, length: u64) -> Result<Vec<u8>>;

    fn get_digester(&self, algorithm: DigestAlgorithm) -> Result<Box<dyn Digester>>;

    fn digest(&self, message: &[u8], algorithm: DigestAlgorithm) -> Result<Vec<u8>>;

    fn new_signer(&self, ski: &[u8], algorithm: DigestAlgorithm) -> Result<Box<dyn Signer>>;

    fn new_verifier(&self, ski: &[u8], algorithm: DigestAlgorithm) -> Result<Box<dyn Verifier>>;
}

#[derive(Debug)]
#[repr(C)]
pub enum DigestAlgorithm {
    SHA256,
    SHA512,
}

pub trait Digester {
    fn init(&mut self) -> Result<()>;
    
    fn update(&mut self, data: &[u8]) -> Result<()>;

    fn digest(&mut self) -> Result<Vec<u8>>;
}

// Signer is an interface which wraps the sign method.
pub trait Signer {
    // sign signs message bytes and returns the signature or an error on failure.
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>>;
}

// Verifier is an interface which wraps the verify method.
pub trait Verifier {
    // verify verifies signature of message and returns the result.
    fn verify(&self, signature: &[u8], message: &[u8]) -> Result<bool>;
}

// Serializer is an interface which wraps the Serialize function.
pub trait Serializer {
    // serialize converts an identity to bytes.  It returns an error on failure.
    fn serialize(&self) -> Result<Vec<u8>>;
}

pub trait SignerSerializer: Signer + Serializer {}
